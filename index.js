#!/usr/bin/env node

// import {Ncludo} from "src/Ncludo";
//ES6 imports are not supported by NodeJS by default.

const Ncludo = require("./src/Ncludo");

const chalk = require("chalk");
const cli = require("commander");
const arguments = process.argv.slice(2);

const currentVersion = "0.0.1";

getVersion();

cli
  .version(`v${currentVersion}`)
  .parse(process.argv);

if (arguments.length === 1 && arguments[0].endsWith(".html")) {
  Ncludo.start(arguments);
} else {
  console.log(`${chalk.red("Error:")} Unrecognized input`);
  cli.outputHelp();
}

function getVersion() {
  console.log(`${chalk.cyan.bold("ncludo")} v${currentVersion}`);
}