const fs = require("fs");
const path = require("path");

class FileUtils {

  openSync(filePath) {
    return fs.readFileSync(path.resolve(process.cwd(), filePath)).toString();
  }

}

module.exports = new FileUtils();