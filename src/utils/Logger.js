const chalk = require("chalk");

class Logger {

  constructor(parentClass) {
    this.className = (typeof parentClass === "string") ? parentClass : parentClass.constructor.name;
  }

  createFormattedMessage(message) {
    return this.getCurrentTime() + ` ${chalk.cyan.bold(this.className)} ${message}`;
  }

  getCurrentTime() {
    let date = new Date();
    let dateString = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
    return `${chalk.gray("[" + dateString + "]")}`;
  }

  log(message) {
    console.log(this.createFormattedMessage(message));
  }

  error(message) {
    console.error(this.createFormattedMessage(message));
  }

}

module.exports = Logger;