// import {Logger} from "./utils/Logger";
// import {FileUtils} from "./utils/FileUtils";
// import {Analyzer} from "./classes/analyzer/Analyzer";
// import {Transpiler} from "./classes/transpiler/Transpiler";
// ES6 imports are not supported by NodeJS by default.

const Logger = require("./utils/Logger");
const FileUtils = require("./utils/FileUtils");
const Analyzer = require("./classes/analyzer/Analyzer");
const Transpiler = require("./classes/transpiler/Transpiler");

class Ncludo {

  constructor() {
    this.logger = new Logger(this);
  }

  start(files) {
    for (let file of files) {
      this.prepareAnalysis(file);
    }
  }

  prepareAnalysis(file) {
    this.logger.log(`Analyzing ${file}`);
    let fileContents = FileUtils.openSync(file);   
    let includes = Analyzer.startAnalysis(fileContents);

    // TODO: Figure out how to replace the includes in the content
    // TODO: forward the transpiled content back to the analyzer to see if there's more to transpile :)
  }

}

module.exports = new Ncludo();