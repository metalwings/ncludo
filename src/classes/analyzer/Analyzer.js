const xml2js = require("xml2js").parseString;
const Logger = require("../../utils/Logger");

class Analyzer {

  constructor() {
    this.logger = new Logger(this);
  }

  startAnalysis(fileContents) {
    let includes = [];

    xml2js(fileContents, (err, result) => {
      if (err) {
        throw this.logger.error(err);
      }
      includes = this.findAllIncludeTags(result);
    });

    return includes;
  }

  findAllIncludeTags(results, includes = []) {
    for (let key in results) {
      let element = results[key];
      if (key === 'include') {
        this.logger.log(" (debug) <include>-Element was found");
        includes.push(element);
      } else if ( typeof results === "object" ) {
        includes = includes.concat(this.findAllIncludeTags(element));
      }
    }
    return includes;
  }

}

module.exports = new Analyzer();